Name:              vino	
Version:           3.22.0
Release:           12
Summary:           Vino is the GNOME desktop sharing server.

License:           GPLv2+	
URL:               https:wiki.gnome.org/Projects/Vino
Source0:           https://download.gnome.org/sources/%{name}/3.22/%{name}-%{version}.tar.xz

Patch0:            Return-error-if-X11-is-not-detected.patch
Patch1:            Do-not-restart-service-after-unclean-exit-code.patch
Patch2:            Do-not-listen-all-if-invalid-interface-is-provided.patch

BuildRequires:     desktop-file-utils gcc gettext intltool libXdamage-devel
BuildRequires:     libXt-devel libXtst-devel libgcrypt-devel avahi-devel libsecret-devel
BuildRequires:     avahi-glib-devel gnutls-devel gtk3-devel libICE-devel libnotify-devel
BuildRequires:     libSM-devel systemd
Requires:          dbus glibc pango
%{?systemd_requires}

%description
A VNC Server for GNOME

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-silent-rules --with-avahi --with-secret  -with-gnutls --without-telepathy
%make_build 

%install
%make_install 

%check
make check
desktop-file-validate %{buildroot}%{_datadir}/applications/vino-server.desktop

%post
%systemd_user_post vino-server.service

%preun
%systemd_user_preun vino-server.service

%postun
%systemd_user_postun vino-server.service

%files
%defattr(-,root,root)
%doc README AUTHORS
%license COPYING
%{_libexecdir}/*
%{_datadir}/locale/*
%{_datadir}/applications/*
%{_datadir}/glib-2.0/schemas/*.xml
%{_userunitdir}/vino-server.service

%files             help
%defattr(-,root,root)
%doc NEWS docs/TODO docs/remote-desktop.txt
%exclude %{_datadir}/dbus-1/services/org.freedesktop.Telepathy.Client.Vino.service

%changelog
* Sun Sep 1 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.22.0-12
- Package init
